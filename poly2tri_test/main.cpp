#include <iostream>
#include "geometry.h"
#include "parse.h"

int main(int argc, char** argv)
{
	Options options;
        parse(argc, argv, options);

        string filename=argv[options.fileindex]; //input bdm file name; 	
        Polygon poly(filename,options.parsebdm);
	poly.setDebugOption(options.debug);      //set debug flag;
       	
	poly.triangulation();                    //main triangulation function
	
	                                         //output results;   
	if(options.showme) poly.saveAsShowme();
	if(options.metapost) poly.saveAsMetaPost();
	if(options.tecplot) poly.saveAsTecplot();

	return 1;
}